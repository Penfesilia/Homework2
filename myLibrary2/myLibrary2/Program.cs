﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace library
{
    class Program
    {
        static void Main(string[] args)
        {
            Library libr = new Library();
            libr.FillLibrary();
            libr.CreateDepartments();
            Author.MaxAuthor(Library.Books);
            libr.MaxDep();
            Book.MinPage(Library.Books);
            // Library.sort(Library.Books);
            WorkWithXML.SaveToXML();
            WorkWithXML.AddToXML(new Book("Дiвчина в перекладі", "Зарубiжна лiтература", "Джин Квок", 701));
            //WorkWithXML.ReadFromXML();
            WorkWithXML.UpdateXML();
            WorkWithXML.RemoveFromXML(new Author("Ольга Кобилянська"));
        }
    }
}





