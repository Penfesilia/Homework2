﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace library
{
    class Library : ICountingBooks
    {//список з усіма книгами бібліотеки
        static private List<Book> books = new List<Book>();
        public void FillLibrary()
        {
            Books.Add(new Book("Дiти капiтана Гранта", "Зарубiжна лiтература", "Жюль Верн", 564));
            Books.Add(new Book("За 80 днiв навколо свiту", "Зарубiжна лiтература", "Жюль Верн", 801));
            Books.Add(new Book("Один день", "Зарубiжна лiтература", "Девiд Нiколсон", 453));
            Books.Add(new Book("Тягар людських пристрастей", "Зарубiжна лiтература", "Сомерсет Моем", 611));
            Books.Add(new Book("Театр", "Зарубiжна лiтература", "Сомерсет Моем", 753));
            Books.Add(new Book("Вiзерунковий покрив", "Зарубiжна лiтература", "Сомерсет Моем", 796));
            Books.Add(new Book("Чорна рада", "Українська лiтература", "Пантелеймон Кулiш", 489));
            Books.Add(new Book("Кобзар", "Українська лiтература", "Тарас Шевченко", 500));
            Books.Add(new Book("Земля", "Українська лiтература", "Ольга Кобилянська", 433));
            Books.Add(new Book("В недiлю рано зiлля копала", "Українська лiтература", "Ольга Кобилянська", 447));
            Books.Add(new Book("Тигролови", "Українська лiтература", "Iван Багряний", 390));
            Books.Add(new Book("Microsoft Visual C#", "IT", "Джон Шарп", 848));
            Books.Add(new Book("Фiлософiя Java", "IT", "Брюс Еккель", 1050));
            Books.Add(new Book("Изучаем Python", "IT", "Марк Лутц", 948));
            Books.Add(new Book("Мужчины с Марса, женщины с Венеры", "Психологiя", "Джон Грей", 848));
            Books.Add(new Book("Жовтий князь", "Українська лiтература", "Василь Барка", 411));
            Books.Add(new Book("Дiвчина в перекладі", "Зарубiжна лiтература", "Джин Квок", 701));
            Books.Add(new Book("Мистецтво любити", "Психологiя", "Ерiх Фромм", 835));
            Books.Add(new Book("7 звичок надзвичайно ефективних людей", "Психологiя", "Стiвен Ковi", 407));
            Books.Add(new Book("Как перестать беспокоиться и начать жить", "Психологiя", "Дейл Карнегi", 512));

        }
        private Department IT = new Department("IT", "Doroshenka");
        private Department worldLiterature = new Department("Зарубiжна лiтература", "Drahomanova street");
        private Department ukrLiterature = new Department("Українська лiтература", "Universitetska street");
        private Department psychology = new Department("Психологiя", "Doroshenka");
        //заповнюємо відділи
        public void CreateDepartments()
        {
            worldLiterature.BookOfDep = new List<Book>();
            var temp = from b in books
                       where b.titleOfDep.Equals("Зарубiжна лiтература")
                       select b;
            worldLiterature.BookOfDep = temp as List<Book>;
            ukrLiterature.BookOfDep = new List<Book>();
            var temp2 = from b in books
                        where b.titleOfDep.Equals("Українська лiтература")
                        select b;
            ukrLiterature.BookOfDep = temp2 as List<Book>;
            IT.BookOfDep = new List<Book>();
            var temp3 = from b in books
                        where b.titleOfDep.Equals("IT")
                        select b;
            IT.BookOfDep = temp3 as List<Book>;
            psychology.BookOfDep = new List<Book>();
            var temp4 = from b in books
                        where b.titleOfDep.Equals("Психологiя")
                        select b;
            psychology.BookOfDep = temp4 as List<Book>;
        }

        private List<Department> d = new List<Department>();
        void FillD()
        {
            d.Add(worldLiterature);
            d.Add(ukrLiterature);
            d.Add(IT);
            d.Add(psychology);
        }



        internal static List<Book> Books { get => books; set => books = value; }

        //метод для знаходження найбільшого відділу
        public void MaxDep()
        {
            FillD();
            Department max = this.d[0];
            for (int i = 1; i < 4; i++)
            {
                if (d[i].CompareTo(max) > 0)
                    max = d[i];
            }
            Console.WriteLine("В нашiй бiблiотецi набiльше книг у вiддiлi: " + max.ToString());
        }
        public int count()
        {
            return Books.Count;
        }
        // Сортування з допомогою LINQ
        public static void sort(List<Book> l)
        {
            var sortedLibrary = from b in l
                                orderby b.Volume
                                select b;
            // Пошук з допомогою LINQ
            Console.WriteLine("Найменше сторiнок у книзi " + sortedLibrary.First().ToString());

        }

    }
}

