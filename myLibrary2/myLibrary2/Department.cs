﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace library
{
    class Department : PartOfLibrary, IComparable, ICountingBooks
    {
        private string titleOfDep;
        private List<Book> bookOfDep;

        public string TitleOfDep { get => titleOfDep; set => titleOfDep = value; }
        internal List<Book> BookOfDep { get => bookOfDep; set => bookOfDep = value; }

        public int CompareTo(object dep)
        {
            return this.count() - ((Department)dep).count();
        }
        public int count()
        {
            // використання LINQ
            var count = Library.Books.Count(book => titleOfDep.Equals(book.titleOfDep));
            return count;
        }
       
        public override string ToString()
        {
            return this.TitleOfDep;
        }
        public Department(string name, string location) :
            base(location)
        {
            titleOfDep = name;
                   
        }

    }

}
